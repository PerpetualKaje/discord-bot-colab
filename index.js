const Discord = require("discord.js") //Discord.js
const config = require("./config.json") //Location of json file
const bot = new Discord.Client(); //The discord bot client
const fs = require("fs"); //Filesystem
bot.commands = new Discord.Collection(); //Discord Collection of commands we can use

fs.readdir("./commands/", (err, files) =>  //Find the commands folder
{

  if(err) console.log(err); //If we get an error when going thorught the commands folder, log it in the console

  let jsfile = files.filter(f => f.split(".").pop() === "js"); //Checks for .js files
  if(jsfile.length <= 0) //There were no .js files js.length = 1 file and the amount was equal or less than 0
  {
    console.log("Couldn't find commands."); //Send a console log
    return; //So we don't loop
  }

jsfile.forEach((f, i) => //For each .JS File we find
{
  let props = require(`./commands/${f}`); //${f} is the js files being defined
  console.log(`${f} loaded!`); //This sends a message in the cmd that a file was read and has 0 errors
  bot.commands.set(props.help.name, props); //We load them
});

});


bot.on("ready", () =>  //The bot is on or ready
{
  console.log(bot.user.username + " is online.") //This is the console log message (bot.user.username will get the bot's name)
});

bot.on("message", async message =>
{
  //a little bit of data parsing/general checks
  if(message.author.bot) return; //So the bot doesn't reply to it's own messages
  if(message.channel.type === 'dm') return; //If the bot was messaged in the dm, ignore it (no commands in dms)
  let content = message.content.split(" "); //Content is the value after the command and we want to define a space
  let command = content[0]; //Command being the first value
  let args = content.slice(1); //The value or variable being the second
  let prefix = config.prefix; //This is just the prefix to start a command

  //checks if message contains a command and runs it
  let commandfile = bot.commands.get(command.slice(prefix.length));
  if(commandfile) commandfile.run(bot,message,args);
})


bot.login(config.token)
