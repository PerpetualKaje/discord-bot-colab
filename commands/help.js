const Discord = require("discord.js");

module.exports.run = async(bot, message, args) =>
{
    if (args[0] == "Admin" || args[0] == "admin")
    {
      if(!message.member.hasPermission("ADMINISTRATOR")) return message.channel.send("Invalid Permission: ADMINISTRATOR"); //Checks if the user has admin permissions

      let helpEmbed = new Discord.RichEmbed() //Creates the embeded message
      .setTitle("HELP") //Sets the title of the embed message
      .setColor("#4286f4") //Hexcolor
      .addField("Ban", "This will ban a user") //Field

      message.channel.send(helpEmbed); //This will send the message
      console.log(`Admin Help Message was sent`); //We can tell the difference between each message sent
    }
    if (args[0] == "Basic" || args[0] == "basic")
    {
      let helpEmbed = new Discord.RichEmbed() //Creates the embeded message
      .setTitle("HELP") //Sets the title of the embed message
      .setColor("#4286f4") //Hexcolor
      .addField("Ping", "The ping of the bot") //Field

      message.channel.send(helpEmbed); //This will send the message
      console.log(`Basic Help Message was sent`); //We can tell the difference between each message sent
    }
    if (args[0] == undefined) //They did not leave an argument to use
    {
      let helpEmbed = new Discord.RichEmbed() //Creates the embedded message
      .setTitle("HELP") //Sets the title of the embeded message
      .setColor("#4286f4") //Hexcolor
      .addField("Admin", "Type Admin") //Field
      .addField("Basic", "Type Basic") //Field

      message.channel.send(helpEmbed); //This will send the message
      console.log(`Help list was sent`); //We can tell the difference between each message sent
    }
}

module.exports.help =
{
  name: "help"
}
