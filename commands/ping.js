const Discord = require("discord.js"); //Loads the discord node module (or discord.js)

module.exports.run = async (bot, message, args) =>
{
  //We are using this to messure the bot's ping, and the bot will say pong
  const m = await message.channel.send("Pong"); //Definition of m

  let pingEmbed = new Discord.RichEmbed() //Creates an embedding for the bot to use

  .setDescription("Ping Information")   //Sets Embed Title

  .setColor("#15f153") //Sets the color to green (Hex color)

  //We are getting the time from the message that was created (pong) and subtracting it with the current message
  .addField("Ping MS", m.createdTimestamp - message.createdTimestamp, true) //The pong message time should be longer than the time that we sent this embed message

  //Writes the actual bot ping (We just look at the bot's latency like any other server or game)
  .addField("API Latency", Math.round(bot.ping), true); //Math.round is just round a decimal to a whole number (bot's definition is shown in index.js)

  message.channel.send(pingEmbed); //Send the embed message into the current channel the bot is in
  console.log(`Ping Command was sent`);
}

//command name
module.exports.help = {
  name: "ping"
}
