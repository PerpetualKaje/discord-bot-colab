const Discord = require("discord.js");

//Checking for commands
module.exports.run = async(bot, message, args) =>
{
  /*
  1. Get the username
  1. We get the mentions
  1. We get the first username that was mentioned example: @mentioned user
  */
  let banneduser = message.guild.member(message.mentions.users.first()); //This is a definition
  if (!banneduser) return message.channel.send("Can't find user");

  let banreason = args.join(" ").slice(22); //Slices the message behind the reason to get the reason and stores it in a "arg" or "args"
  if (!banreason) return message.channel.send("Please add a reason");

  //Checks if the user has the permission to ban members
  if (!message.member.hasPermission("BAN_MEMBERS")) return message.channel.send("Invalid Permission: BAN_MEMBERS");

  //Creates the kick embed message for the logs
  let banEmbed = new Discord.RichEmbed()
  .setDescription("Bans") //You could use title, but I prefer SetDescription because it has a small font size
  .setColor("#FF0000") //Hexcolor(rr, gg, bb)
  .addField("Banned User", `${banneduser} with ID ${banneduser.id}`) //The banned user information is shown using stored variables
  .addField("Banned By", `<@${message.author.id}> with ID ${message.author.id}`) //This mentions the author using an id, showing up as a username instead
  .addField("Banned In", message.channel) //message channel will get the channel that the message was sent inspect
  .addField("Banned Time", message.createdAt) //Message time is found using a property with the message class
  .addField("Reason", banreason);

   //Define adming log channel
  let adminlogchannel = message.guild.channels.find(`name`, "admin_logs"); //Name is a property, and the admin_logs is the value of the property we are trying find
  if (!adminlogchannel) return message.channel.send("Channel not found");

  //This will ban the user
  message.guild.member(banneduser).ban(banreason);

  adminlogchannel.send(banEmbed); //We will use the defined adminlog channel and send the embed in there
  message.channel.send(`Cya ${bUser} @ Media`);
}

module.exports.help = {
  name:"ban"
}
