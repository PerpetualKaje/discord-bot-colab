const Discord = require("discord.js"); //Discord.js

module.exports.run = async(bot, message, args) =>
{
  let hugmentioneduser = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0])); //gets the mentioned user
  if (!hugmentioneduser) return message.channel.send(`Can't find user`); //Can't find the user

  let huggifembed = new Discord.RichEmbed() //Creates the embed
  .setColor("#ff9900") //Hexcolor
  .setDescription(`${message.author} has hugged ${hugmentioneduser}`) //This finds the message author and the mentioned user
  //This will set the image to the local images in the folder
  .setImage(`../hug/${Math.floor(Math.random() * 37) + 1}.gif`); //replace 8 with the number of pictures that you have
  message.channel.send(huggifembed);
}

module.exports.help = {
  name: "hug"
}
